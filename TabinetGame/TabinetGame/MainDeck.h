#pragma once
#include <vector>
#include "Card.h"
#include <time.h>
#include <stdlib.h>
#include "Player.h"
#include "Game.h"
#include <algorithm>
#include <random>


class MainDeck
{
public:

	void setMainDeck(const Card& card);
	void createMainDeck(const std::vector<Card> cards);
	void setTableDeck(const Card& card);
	void createTableDeck(const std::vector<Card> cards);
	int getMainDeckSize()const;
	int getTableDeckSize() const;
	Card getCardFromPosTable(const int& position)const;

	void InitMainDeck();
	void InitTableDeck();
	Card PickRandomCard();
	void FillPlayerDeck(Player& player, const int& number_of_cards);
	void CoutMainDeck();
	void CoutTableDeck();
	//bool CheckSumPoints(Player& player, const int& option, const std::vector<int> options);


private:
	std::vector<Card> m_main_deck;
	std::vector<Card> m_table_deck;

};

