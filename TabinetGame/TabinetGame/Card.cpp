#include "Card.h"

Card::Card(const Number number, const Type type)
	:m_number(number),m_type(type)
{
}


Card::Number Card::GetNumber() const
{
	return this->m_number;
}

Card::Type Card::GetType() const
{
	return this->m_type;
}

void Card::SetType(const Type& type)
{
	m_type = type;
}
void Card::SetType(const int& type)
{
	m_type = static_cast<Type>(type);
}
void Card::SetValue(const Number& value)
{
	m_number = value;
}
void Card::SetValue(const int& value)
{
	m_number = static_cast<Number>(value);
}

std::ostream& operator<<(std::ostream& flux,const Card& card)
{
	switch (card.m_number)
	{
	case Card::Number::Ace:
		flux << "A_";
		break;
	case Card::Number::Jack:
		flux << "J_";
		break;
	case Card::Number::Queen:
		flux << "Q_";
		break;

	case Card::Number::King:
		flux << "K_";
		break;

	default:
		flux << (int16_t)card.m_number<<"_";
		break;
	}
	switch (card.m_type)
	{
	case Card::Type::Hearts:
		flux << "inima";
		break;
	case Card::Type::Diamonds:
		flux << "romb";
		break;
	case Card::Type::Spades:
		flux << "frunza";
		break;
	case Card::Type::Clubs:
		flux << "trefla";
		break;
	default:
		break;
	}

	return flux;
}
