#pragma once
#include <string>
#include <vector>
#include "Card.h"

class Player
{
public:
	Player() = default;
	Player(const std::string& name, const std::vector<Card>& cards = {}, const std::vector<Card>& point_cards = {}, const int &points=0, const int& tables=0);

	int GetPoints() const;
	int GetTables() const;
	std::string GetName() const;
	std::vector<Card> GetCards() const;
	std::vector<Card> GetPointCards() const;
	Card getCardFromPos(const int& position)const;

	void SetPoints(const int& points);
	void SetTables(const int& tables);
	void SetCards(const Card card);
	void SetPointCards(const Card point_card);

	void removeCards();
	void removeCardFromPos(const int& position);

	void CoutPlayer() const;
	void countPoints(const Player& player);
private:
	std::string m_name;
	std::vector<Card> m_cards;
	std::vector<Card> m_point_cards;
	int m_points;
	int m_tables;
};

