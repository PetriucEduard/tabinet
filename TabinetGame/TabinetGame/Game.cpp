#include <iostream>
#include "Game.h"
#include <thread>
#include <chrono>

void Game::InitPlayers(int& players_number)
{
	unsigned option;
	std::cout << "In cati jucatori doriti sa jucati? Jocul se joaca de la 2 la 4 jucatori" << std::endl;
	std::cin >> option;
	switch (option)
	{
	case 2:
	{
		std::cout << std::endl;
		std::string numePlayer;
		std::cout << "Introduceti numele primului jucator: ";
		std::cin >> numePlayer;
		m_player1 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		std::cout << "introduceti numele celui de-al doilea jucator: ";
		std::cin >> numePlayer;
		m_player2 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		players_number = 2;
		std::cout << "Jocul incepe in 2 secunde";
		break;
	}
	case 3:
	{
		std::cout << std::endl;
		std::string numePlayer;
		std::cout << "Introduceti numele primului jucator: ";
		std::cin >> numePlayer;
		m_player1 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		std::cout << "introduceti numele celui de-al doilea jucator: ";
		std::cin >> numePlayer;
		m_player2 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		std::cout << "introduceti numele celui de-al treilea jucator: ";
		std::cin >> numePlayer;
		m_player3 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		players_number = 3;
		std::cout << "Jocul incepe in 2 secunde";
		break;
	}
	case 4:
	{
		std::cout << std::endl;
		std::string numePlayer;
		std::cout << "Introduceti numele primului jucator: ";
		std::cin >> numePlayer;
		m_player1 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		std::cout << "introduceti numele celui de-al doilea jucator: ";
		std::cin >> numePlayer;
		m_player2 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		std::cout << "introduceti numele celui de-al treilea jucator: ";
		std::cin >> numePlayer;
		m_player3 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		std::cout << "introduceti numele celui de-al patrulea jucator: ";
		std::cin >> numePlayer;
		m_player4 = Player(numePlayer);
		std::cout << numePlayer << " salvat" << std::endl;
		players_number = 4;
		std::cout << "Jocul incepe in 2 secunde";
		break;
	}
	default:
		std::cout << "Optiune aleasa gresit" << std::endl;
		Game::InitPlayers(players_number);
		break;
	}
}

/*void Game::FirstHandOption(const int& option)
{
	switch (option)
	{
	case 0:
		Deck.createTableDeck(m_player1.GetCards());
		m_player1.removeCards();
		if (m_players_number == 2)
		{
			Deck.FillPlayerDeck(m_player1, 6);
			Deck.FillPlayerDeck(m_player2, 6);
		}
		else
			if (m_players_number == 3)
			{
				Deck.FillPlayerDeck(m_player1, 6);
				Deck.FillPlayerDeck(m_player2, 6);
				Deck.FillPlayerDeck(m_player3, 6);
			}
			else
				if (m_players_number == 4)
				{
					Deck.FillPlayerDeck(m_player1, 6);
					Deck.FillPlayerDeck(m_player2, 6);
					Deck.FillPlayerDeck(m_player3, 6);
					Deck.FillPlayerDeck(m_player4, 6);

				}
		break;

	case 1:
		Deck.InitTableDeck();
		if (m_players_number == 2)
		{
			Deck.FillPlayerDeck(m_player1, 2);
			Deck.FillPlayerDeck(m_player2, 6);
		}
		else
			if (m_players_number == 3)
			{
				Deck.FillPlayerDeck(m_player1, 2);
				Deck.FillPlayerDeck(m_player2, 6);
				Deck.FillPlayerDeck(m_player3, 6);
			}
			else
				if (m_players_number == 4)
				{
					Deck.FillPlayerDeck(m_player1, 2);
					Deck.FillPlayerDeck(m_player2, 6);
					Deck.FillPlayerDeck(m_player3, 6);
					Deck.FillPlayerDeck(m_player4, 6);
				}
		break;
	default:
		std::cout << "Optiune aleasa gresit, Alegeti dintre 0 si 1" << std::endl;
		break;

	}
}*/

void Game::PlayerTurn(Player& player)
{
	std::cout << player.GetName() << " este randul tau." << std::endl;
	player.CoutPlayer();
	std::cout << std::endl;
}


void Game::Run()
{
	MainDeck Deck;
	Deck.InitMainDeck();
	int players_number;
	Game::InitPlayers(players_number);
	m_players_number = players_number;
	std::this_thread::sleep_for(std::chrono::seconds(2));
	system("cls");
	//Deck.CoutMainDeck(); //test init deck
	Deck.FillPlayerDeck(m_player1, 4);
	std::cout << std::endl;
	m_player1.CoutPlayer();
	std::cout << std::endl << m_player1.GetName() << " pastrezi mana sau o dai jos?";
	std::cout << std::endl << " 0 - da jos, 1 - pastreaza. Alegerea este: ";
	int option;
	std::cin >> option;
	switch (option)
	{
	case 0:
		Deck.createTableDeck(m_player1.GetCards());
		m_player1.removeCards();
		if (m_players_number == 2)
		{
			Deck.FillPlayerDeck(m_player1, 6);
			Deck.FillPlayerDeck(m_player2, 6);
		}
		else
			if (m_players_number == 3)
			{
				Deck.FillPlayerDeck(m_player1, 6);
				Deck.FillPlayerDeck(m_player2, 6);
				Deck.FillPlayerDeck(m_player3, 6);
			}
			else
				if (m_players_number == 4)
				{
					Deck.FillPlayerDeck(m_player1, 6);
					Deck.FillPlayerDeck(m_player2, 6);
					Deck.FillPlayerDeck(m_player3, 6);
					Deck.FillPlayerDeck(m_player4, 6);

				}
		break;

	case 1:
		Deck.InitTableDeck();
		if (m_players_number == 2)
		{
			Deck.FillPlayerDeck(m_player1, 2);
			Deck.FillPlayerDeck(m_player2, 6);
		}
		else
			if (m_players_number == 3)
			{
				Deck.FillPlayerDeck(m_player1, 2);
				Deck.FillPlayerDeck(m_player2, 6);
				Deck.FillPlayerDeck(m_player3, 6);
			}
			else
				if (m_players_number == 4)
				{
					Deck.FillPlayerDeck(m_player1, 2);
					Deck.FillPlayerDeck(m_player2, 6);
					Deck.FillPlayerDeck(m_player3, 6);
					Deck.FillPlayerDeck(m_player4, 6);
				}
		break;
	default:
		std::cout << "Optiune aleasa gresit, Alegeti dintre 0 si 1" << std::endl;
		std::cin >> option;
		break;

	}
	std::this_thread::sleep_for(std::chrono::seconds(2));
	system("cls");
	if (m_players_number == 2)
	{
		m_player1.CoutPlayer();
		m_player2.CoutPlayer();
	}
	if (m_players_number == 3)
	{
		m_player1.CoutPlayer();
		m_player2.CoutPlayer();
		m_player3.CoutPlayer();
	}
	if (m_players_number == 4)
	{
		m_player1.CoutPlayer();
		m_player2.CoutPlayer();
		m_player3.CoutPlayer();
		m_player4.CoutPlayer();
	}
	Deck.CoutTableDeck(); // test afisari pasul 1
	std::cout << std::endl;
	Deck.CoutMainDeck();
	std::cout << std::endl;
	/*while (Deck.getMainDeckSize() % 2 >= 1)
	{
		system("cls");
		Game::PlayerTurn(m_player1);
		Deck.CoutTableDeck();
		std::cout << std::endl;
		std::cout << "Optiuni:" << std::endl;
		std::cout << " 0 - dau carte jos" << std::endl;
		std::cout << " 1 - fac puncte" << std::endl;
		std::cin >> option;
		switch (option)
		{
		case 0:
			std::cout << "Pe ce pozitie se afla cartea pe care o vei da jos?";
			int optiune_carte_jos;
			std::cin >> optiune_carte_jos;
			Deck.setTableDeck(m_player1.getCardFromPos(optiune_carte_jos));
			m_player1.removeCardFromPos(optiune_carte_jos);
		case 1:
			std::cout << "Ce carte din pachet alegi? Introdu pozitia ";
			int optiune_carte_pachet_puncte;
			std::cin >> optiune_carte_pachet_puncte;
			std::cout << std::endl << "Daca cartea aleasa pentru a fi adunata este un Ace, te rog sa alegi prima data. daca exista pe tabla, cartea Ace";
			std::cout << std::endl << "Ce carti alegi sa aduni de pe tabla? Introdu pozitiile";
			std::vector<int> options;
			int pozitii_carti;
			options.push_back(pozitii_carti);
			while (std::cin >> pozitii_carti)
			{
				options.push_back(pozitii_carti);
			}


		}
	}*/
}