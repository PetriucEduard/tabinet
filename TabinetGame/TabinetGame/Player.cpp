#include "Player.h"

Player::Player(const std::string& name, const std::vector<Card>& cards, const std::vector<Card>& point_cards, const int& points, const int& tables)
	:m_name(name), m_cards(cards),m_point_cards(point_cards),m_points(points),m_tables(tables)
{
}

int Player::GetPoints() const
{
	return this->m_points;;
}

int Player::GetTables() const
{
	return this->m_tables;
}

std::string Player::GetName() const
{
	return m_name;
}

std::vector<Card> Player::GetCards() const
{
	return m_cards;
}

std::vector<Card> Player::GetPointCards() const
{
	return this->m_point_cards;
}

Card Player::getCardFromPos(const int& position) const
{
	return m_cards[position];
}

void Player::SetPoints(const int& points)
{
	this->m_points = points;
}

void Player::SetTables(const int& tables)
{
	this->m_tables = tables;
}

void Player::SetCards(const Card card)
{
	this->m_cards.emplace_back(card);
}

void Player::SetPointCards(const Card point_card)
{
	this->m_point_cards.emplace_back(point_card);
}

void Player::removeCards()
{
	m_cards.clear();
}

void Player::removeCardFromPos(const int& position)
{
	m_cards.erase(m_cards.begin() + position, m_cards.begin() + position + 1);
}

void Player::CoutPlayer() const
{
	std::cout << std::endl;
	std::cout << "Jucatorul " << m_name << " are urmatoarele carti:" << std::endl;
	std::cout << "-----------------------------------" << std::endl;
	for (unsigned contor = 0; contor < m_cards.size(); contor++)
	{
		std::cout << m_cards[contor] << ' ';
	}
	std::cout<<std::endl << "-----------------------------------" << std::endl;
}

void Player::countPoints(const Player& player)
{
	for (unsigned contor = 0; contor < m_point_cards.size(); ++contor)
	{
		if (m_point_cards[contor].GetType() == Card::Type::Diamonds && m_point_cards[contor].GetNumber() == Card::Number::Ten) //zece de romb, Vacuta
			m_points += 2;
		if (m_point_cards[contor].GetType() == Card::Type::Clubs && m_point_cards[contor].GetNumber() == Card::Number::Two) //doi de trefla
			m_points += 2;
		if (m_point_cards[contor].GetType() == Card::Type::Clubs && m_point_cards[contor].GetNumber() != Card::Number::Two) //carte de trefla 1 punct
			m_points += 1;
		if (m_point_cards[contor].GetNumber() == Card::Number::Ten && m_point_cards[contor].GetType()!=Card::Type::Diamonds) //carte de 10
			m_points += 1;
		if (m_point_cards[contor].GetNumber() == Card::Number::Jack) //carte de valet
			m_points += 1;
		if (m_point_cards[contor].GetNumber() == Card::Number::Queen) //carte de dama
			m_points += 1;
		if (m_point_cards[contor].GetNumber() == Card::Number::King) //carte de rege
			m_points += 1;
	}
	m_points += m_tables; //tabla
}