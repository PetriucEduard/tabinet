#pragma once
#include <cstdint>
#include <iostream>

class Card
{
public:
	enum class Type
	{
		Hearts=1, //inima
		Diamonds=2, //romb
		Spades=3, //frunza
		Clubs=4 //trefla
	};
	
	enum class Number
	{
		Ace=1,
		Two=2,
		Three=3,
		Four=4,
		Five=5,
		Six=6,
		Seve=7,
		Eight=8,
		Nine=9,
		Ten=10,
		Jack=12,
		Queen=13,
		King=14
	};

public:
	Card() = default;
	Card(const Number number, const Type type);


	Number GetNumber() const;
	Type GetType() const;

	void SetType(const Type& type);
	void SetType(const int& type);
	void SetValue(const Number& value);
	void SetValue(const int& value);

	friend std::ostream& operator<<(std::ostream& flux,const Card& card);

private:
	Number m_number;
	Type m_type;
};

