#include "MainDeck.h"

void MainDeck::setMainDeck(const Card& card)
{
	this->m_main_deck.emplace_back(card);
}

void MainDeck::createMainDeck(const std::vector<Card> cards)
{
	for (int contor = 0; contor < cards.size(); ++contor)
	{
		setMainDeck(cards[contor]);
	}
}

void MainDeck::setTableDeck(const Card& card)
{
	this->m_table_deck.emplace_back(card);
}

void MainDeck::createTableDeck(const std::vector<Card> cards)
{
	for (int contor = 0; contor < cards.size(); ++contor)
	{
		setTableDeck(cards[contor]);
	}
}

int MainDeck::getMainDeckSize() const
{
	return m_main_deck.size();
}

int MainDeck::getTableDeckSize() const
{
	return m_table_deck.size();
}

Card MainDeck::getCardFromPosTable(const int& position) const
{
		return m_table_deck[position];
}

void MainDeck::InitMainDeck()
{
	srand(time(NULL));
	srand((unsigned)time(0));
	for(unsigned contor=1;contor<=4;contor++)
		for (unsigned contor2 = 1; contor2 <= 14; contor2++)
		{
			if (contor2 == 11)
				contor2++;
			Card card;
			card.SetType(contor);
			card.SetValue(contor2);
			m_main_deck.push_back(card);

		}
	std::srand(time(0));
	auto rng = std::default_random_engine(rand());
	std::shuffle(m_main_deck.begin(), m_main_deck.end(), rng);
}

void MainDeck::InitTableDeck()
{
	for (unsigned contor = 0; contor < 4; contor++)
		m_table_deck.emplace_back(PickRandomCard());
}

Card MainDeck::PickRandomCard()
{
	int random;
	random = rand() % m_main_deck.size() + 1;
	Card auxiliar;
	auxiliar = m_main_deck[random];
	m_main_deck.erase(m_main_deck.begin()+random, m_main_deck.begin()+random+1);
	return auxiliar;
}

void MainDeck::FillPlayerDeck(Player& player, const int& number_of_cards)
{
	for (unsigned contor = 0; contor < number_of_cards; contor++)
	{
		player.SetCards(PickRandomCard());
	}

}

void MainDeck::CoutMainDeck()
{
	std::cout << "MainDeck-ul are urmatoarele carti:" << std::endl;
	for (unsigned contor = 0; contor < m_main_deck.size(); contor++)
		std::cout << m_main_deck[contor] << ' ';
}

void MainDeck::CoutTableDeck()
{
	std::cout << "TableDeck-ul are urmatoarele carti:" << std::endl;
	for (unsigned contor = 0; contor < m_table_deck.size(); contor++)
		std::cout << m_table_deck[contor] << ' ';
}

/*bool MainDeck::CheckSumPoints(Player& player, const int& option, const std::vector<int> options)
{
	if (player.getCardFromPos(option).GetNumber == getCardFromPosTable(options[0]) && player.getCardFromPos(option).GetNumber == Card::Number::Ace)
		return true;
	int table_sum = 0;
	for (unsigned contor = 0; contor < options.size(); contor++)
	{
		table_sum = table_sum + (int)getCardFromPosTable(options[contor]).GetNumber();
	}
	if (table_sum == 11 && player.getCardFromPos(option).GetNumber() == Card::Number::Ace)
		return true;
	else
		if (table_sum == 11 && player.getCardFromPos(option).GetNumber() != Card::Number::Ace)
			return false;
	if (table_sum == (int)player.getCardFromPos(option).GetNumber())
		return true;
	return false;
}*/

