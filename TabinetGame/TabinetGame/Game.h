#pragma once
#include "Card.h"
#include "Player.h"
#include "MainDeck.h"

class Game
{

public:

	void InitPlayers(int &players_number);
	//void FirstHandOption(const int& option);
	void PlayerTurn(Player& player);
	void Run();


public:
	int m_players_number;
	Player m_player1;
	Player m_player2;
	Player m_player3;
	Player m_player4;
};

